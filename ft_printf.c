/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: imedgar <imedgar@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/15 12:05:24 by imedgar           #+#    #+#             */
/*   Updated: 2020/05/15 15:32:07 by imedgar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_printf(const char *format, ...)
{
	const char	*str = format;
	va_list		factor;

	va_start(factor, format);
	while (*str != '\0')
	{
		if (*str == '%')
		{
			if (*++str == 'd')
				ft_putnbr(va_arg(factor, int));
		}
		else
			ft_putchar(*str);
		++str;
	}
	va_end(factor);
	return (0);
}
